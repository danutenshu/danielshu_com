# frozen_string_literal: true

class SiteController < ApplicationController
  def index
  end
  def home
    render action: "index"
  end
  def resume
  end
end
