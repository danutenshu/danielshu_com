# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'site/index'
  get 'index', to: 'site#index'
  get 'home', to: 'site#index', as: 'home'
  get 'projects', to: 'site#projects', as: 'projects'
  get 'resume', to: 'site#resume', as: 'resume'
  root 'site#index'
end
