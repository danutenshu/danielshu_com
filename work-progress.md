# Progression/Thought Process

## October 9th

Finally after tinkering around, taking a week apiece to figure out how to get images to link, the spacing for HTML
elements, I've made headway into my first decent draft of site!

So, having gone through some researching on how to do basic things like change the link color settings
 (color: currentColor), I've also decided what to improve the process so I don't have to repeat things. **Clicking a URL under the subdomain that is not a resource should not have to reload the navigation bar. Unless, it opens  a link to a whitepaper or article I've written, which in that case...**

```TODO: 2x down development, all "application" pages (not resource category, e.g. resume, papers) should not reload the nav bar. Just graceful transparent out the current body (view/page.html).```

That sits somewhere after DNS configuration to ubeicecream.com.

```TODO: Have dynamic project pages that can build icons for projects that are droppable to a project views.```

```TODO: Also, on the note of submitting notes and proper code, I should set strict, business standards. The most basic of which is coding/documenting standards. I should have a Jenkins hook that checks style and code-formatting (on a per-new-language basis). Later on, I'd like all my code to do be FIPS 140-2 approved as well.```

```TODO: DNS configuration```

```TODO: Raspberry Pi hosting```

```TODO: Chaos.Monkey. ```