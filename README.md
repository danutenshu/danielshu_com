# README

A personal website showcasing projects, experience, and ideas, that provides a sandbox for testing UX ideas.
Emphasis on:
* Explaining implementation choices
* Correcting practices to Best Practice standards
* Good documentation (for easier project handoff)
* Web hosting tools performance (NGINX/Apache hybrid)
* Hosting OS compatibility
  - RHEL
  - Ubuntu
* End-user Browser compability
But should not sacrifice:
* My development efficiency
* Good issue tracking
* Benchmark performance test suite
* .sql script
## Deployment
* todo - Instructions on NGINX/Apache hybrid
* Document rto benchmark performance
## Dependencies
* Ruby 5.1.6
* Gems
  - rails 2.1.5
  - rubocop-rails
* (suggestion) tmux and tree
* todo - how is database set up?


## Implementation Choices
* CHANGELOG.md - Included because the project is a showcase for implementation choice and great sandbox Rails site for beginner Rails dev to tear apart/Frankenstein-monster's it.
  - Is this redundant to git commit messages? More work?
  per (keepachangelog.com)[https://keepachangelog.com/en] 
### Routing

## Developer Notes
Majority (60%) criticisms will be grounds for change, unless personal choices anticipate that

